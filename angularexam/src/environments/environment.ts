// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyDL_n9HN9UNVgkELELnRDK5orKWS4ve2yQ",
  authDomain: "newangularexam.firebaseapp.com",
  databaseURL: "https://newangularexam.firebaseio.com",
  projectId: "newangularexam",
  storageBucket: "newangularexam.appspot.com",
  messagingSenderId: "979188096009",
  appId: "1:979188096009:web:893a42485b2ab70cf1acd8",
  measurementId: "G-V41MTV5G08"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
