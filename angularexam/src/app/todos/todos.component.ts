import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos$:Observable<any[]>;
  userId:string;
  panelOpenState = false;
  clicked = false;

  constructor(private todoService:TodoService,
    public authService:AuthService) { }

  ngOnInit() {

    console.log("im in ngInet")  
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.todos$ = this.todoService.getTodos(this.userId); 
      }
    )
  }
done(){
  
}
}
