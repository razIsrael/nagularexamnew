import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addtodo',
  templateUrl: './addtodo.component.html',
  styleUrls: ['./addtodo.component.css']
})
export class AddtodoComponent implements OnInit {
  details:string;
  todo: string;
  id:string;
  userId:string;


  constructor(private todoService:TodoService,
    private authService:AuthService, 
    private router:Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          
          this.todoService.getTodo(this.userId,this.id).subscribe(
          todo => {
            console.log(todo.data().todo)
            console.log(todo.data().details)
            this.todo = todo.data().todo;
            this.details = todo.data().details;})        
      }
   })
  }

  onSubmit(){
      console.log('In onSubmit');
      this.todoService.addTodo(this.userId,this.todo,this.details)
    
    this.router.navigate(['/todo']);     
    
  }
}
