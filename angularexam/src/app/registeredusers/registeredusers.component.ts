import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../user.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registeredusers',
  templateUrl: './registeredusers.component.html',
  styleUrls: ['./registeredusers.component.css']
})
export class RegisteredusersComponent implements OnInit {
  users$:Observable<any>;
  //userId:string;
  //like:number; 


  constructor(private userService: UserService,public auth:AuthService) { }

  ngOnInit() {
    this.users$ = this.userService.getUsersOfTest();
       
    
  }

}
