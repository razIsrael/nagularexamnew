import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,private db:AngularFirestore) { }

  APIUSER = "https://jsonplaceholder.typicode.com/users"
  APITODO = ""

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  todoCollection:AngularFirestoreCollection;
  
  getUsersOfTest():Observable<any[]>{
    this.userCollection = this.db.collection(`users/72dRPQVkEcZ2GSjFpocZpsyPMzf2/users`);
        console.log('user collection created');
        return this.userCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            //data.id = a.payload.doc.id;
            console.log(data);
            return { ...data };
          }))
        ); 
  }


  getUser():Observable<any>{
    return this.http.get<User[]>(this.APIUSER);
  }
  
  adduser(name:string, email:string, uid:string){
    const user = {name: name, email: email ,id: uid};
    console.log( user)
    this.userCollection.doc('myUsers').collection('users').add(user);
  }
 

}
