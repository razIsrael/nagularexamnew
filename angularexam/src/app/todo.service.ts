import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private db: AngularFirestore, private authService:AuthService) { }
    
  userCollection:AngularFirestoreCollection = this.db.collection('users');
    todoCollection:AngularFirestoreCollection;

    getTodos(userId): Observable<any[]> {
      this.todoCollection = this.db.collection(`users/${userId}/todos`);
      console.log('todos collection created');
      return this.todoCollection.snapshotChanges().pipe(
        map(collection => collection.map(document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }))
      );    
    } 

    // getBook(userId, id:string):Observable<any>{
    //   return this.db.doc(`users/${userId}/books/${id}`).get();
    // }
    getTodo(userId, id:string):Observable<any>{
      return this.db.doc(`users/${userId}/todos/${id}`).get();
    }

    // addBook(userId:string, title:string, author:string){
    //   console.log('In add books');
    //   const book = {title:title,author:author}
    //   //this.db.collection('books').add(book)  
    //   this.userCollection.doc(userId).collection('books').add(book);
    // } 
    addTodo(userId:string, todo:string, details:string){
      console.log('In add todos');
      console.log(todo,details);
      const todo1 = {todo:todo, details:details}
      this.userCollection.doc(userId).collection('todos').add(todo1);
    }

    updateBook(userId:string, id:string,title:string,author:string){
      this.db.doc(`users/${userId}/books/${id}`).update(
        {
          title:title,
          author:author
        }
      )
    }

    deleteBook(userId:string, id:string){
      this.db.doc(`users/${userId}/books/${id}`).delete();
    }

}
