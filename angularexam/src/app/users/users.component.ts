import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { UserService } from '../user.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  Users$:Observable<User[]>;
  //comments$:Observable<Comment[]>;
  userId:string;
  text:string;
  name:string;

  constructor(private userService: UserService ,private auth:AuthService) { }

  ngOnInit() {
    this.userService.getUser()
    .subscribe(data =>this.Users$ = data );
    // this.userService.getcomment()
    // .subscribe(data =>this.comments$ = data );

    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }
createAccount(name: string, email: string, uid: string){
  this.auth.signup(email,"12345678");
  this.userService.adduser(name, email, uid);


}


}
