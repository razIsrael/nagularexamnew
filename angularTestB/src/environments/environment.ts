// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCajsLFDh75lz4s1m-LTDdnAFWv0KXgfvM",
    authDomain: "angular-test-b.firebaseapp.com",
    databaseURL: "https://angular-test-b.firebaseio.com",
    projectId: "angular-test-b",
    storageBucket: "angular-test-b.appspot.com",
    messagingSenderId: "519573205148",
    appId: "1:519573205148:web:eed7a7bbe0aea344857d59",
    measurementId: "G-J2SLFFN1ES"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
