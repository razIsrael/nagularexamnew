import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-successlogin',
  templateUrl: './successlogin.component.html',
  styleUrls: ['./successlogin.component.css']
})
export class SuccessloginComponent implements OnInit {

  constructor(public authService:AuthService) { }

  ngOnInit() {
  }

}
