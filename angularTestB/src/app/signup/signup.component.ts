import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService:AuthService, public router:Router) { }

  email: string;
  password: string;

  onSubmit(){
    console.log(this.email, this.password);
    this.authService.signup(this.email, this.password);
    console.log("sent to function");
    this.router.navigate(['/extra']);
  }
  ngOnInit() {
  }

}
