import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;

  constructor(public afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
   }

   signup(email:string, password:string){
    this.afAuth
    .auth.createUserWithEmailAndPassword(email,password)
    .catch(function(error) {
     // Handle Errors here.
     // var errorCode = error.code;
     var errorMessage = error.message;
     alert(errorMessage);
     console.log(error);
   });
  }

  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
    this.router.navigate (['/login'])
  }

  login(email:string, password:string){
    this.afAuth
    .auth.signInWithEmailAndPassword(email,password).then
    (res =>{ this.router.navigate (['/successlogin']),
             console.log('Succesful Login', res)})
    .catch(function(error) {
      // Handle Errors here.
      // var errorCode = error.code;
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
      console.log(errorCode);
    });
    // .then(
      // res => { console.log('Succesful login', res);
  //  }
    // )
  }




}
